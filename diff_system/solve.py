import numpy as np
import matplotlib.pyplot as plt

# Constants
nX = 20
nT = 20
xMax = 1
tMax = 1

x = np.linspace(0, xMax, nX)
t = np.linspace(0, tMax, nT)
y = np.zeros((nX, nT), dtype=float)  # u of solution 1
z = np.zeros((nX, nT), dtype=float)  # u of solution 2
diff = np.zeros((nX, nT), dtype=float)  # difference z-y

y[0, :] = 0
y[:, 0] = x
z[0, :] = 0
z[:, 0] = x

# Number of lines on graphic
LINES = 10


h = float(xMax / (nX - 1))
tau = float(tMax / (nT - 1))

eps = 1e-11


def p(x):
    return np.arctan(x)


def dp(x):
    return 1 / (1 + x * x)


def f(x, a, b):
    return (x - a) / tau + (p(x) - p(b)) / h


def df(x):
    return 1 / tau + dp(x) / h


# solution 1 - Newthon method
def solve(a, b):
    result = b
    d = eps + 1

    while d > eps:
        y = result
        result = y - f(y, a, b) / df(y)
        d = abs(y - result)

    return result


def plottnig():
    tIdx = np.linspace(0, nT, LINES, dtype=int, endpoint=False)

    # solution 1
    legend1 = []

    plt.figure(figsize=(100, 100))

    for i in tIdx:
        plt.plot(x, y[:, i])
        plt.xlabel('x')
        plt.ylabel('u1')
        legend1.append("t = {}".format(t[i]))

    plt.legend(legend1)

    # solution 2
    legend2 = []

    plt.figure(figsize=(100, 100))

    for i in tIdx:
        plt.plot(x, z[:, i])
        plt.xlabel('x')
        plt.ylabel('u2')
        legend2.append("t = {}".format(t[i]))

    plt.legend(legend2)

    # difference
    legend3 = []

    plt.figure(figsize=(100, 100))

    for i in tIdx:
        plt.plot(x, diff[:, i])
        plt.xlabel('x')
        plt.ylabel('difference')
        legend3.append("t = {}".format(t[i]))

    plt.legend(legend3)

    plt.show()


def main():
    # solution 1
    for i in range(1, nX):
        for j in range(1, nT):
            y[i, j] = solve(y[i, j - 1], y[i - 1, j])

    # solution 2
    for i in range(1, nX):
        for j in range(1, nT):
            z[i, j] = tau * ((p(z[i-1, j-1]) - p(z[i, j-1]))/h + z[i, j-1]/tau)

    # difference z-y
    for i in range(0, nX):
        for j in range(0, nT):
            diff[i, j] = z[i, j] - y[i, j]

    plottnig()


if __name__ == "__main__":
    main()
